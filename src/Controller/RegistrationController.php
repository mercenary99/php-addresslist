<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController {
    #[Route('/register', name: 'register')]
    public function index(Request $req, UserPasswordEncoderInterface $passEncoder): Response {
        $user = new User();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($req);

        if ($form->isSubmitted()) {
            $password = $user->getPassword();

            if(empty($password)){
                $this->addFlash('error', 'Passwörter ist nicht gleich');
                return $this->render('registration/index.html', [
                    'userForm' => $form->createView(),
                ]);
            } 

            $user->setPassword(
                $passEncoder->encodePassword($user, $password)
            );

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('login'));
        }

        return $this->render('registration/index.html', [
            'userForm' => $form->createView()
        ]);
    }
}