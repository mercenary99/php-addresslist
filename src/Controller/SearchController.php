<?php

namespace App\Controller;

use App\Repository\AddressRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController {
    #[Route('/search', name: 'search')]
    public function index(Request $req, AddressRepository $ar) {
        $search = $req->request->get('search');
        $userId = $this->getUser()->getId();

        $contex = [
            'addresses' => $ar->findByAnyField($search, $userId),
            'search' => $search
        ];

        return $this->render('address/index.html', $contex);
    }
}