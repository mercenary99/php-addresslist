<?php

namespace App\Form;

use App\Entity\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('firstname', TextType::class, ['label' => 'Vorname'])
            ->add('lastname', TextType::class, ['label' => 'Nachname'])
            ->add('street', TextType::class, [
                'label' => 'Strasse',
                'required' => false
                ])
            ->add('zip', TextType::class, [
                'label' => 'PLZ',
                'required' => false
                ])
            ->add('city', TextType::class, [
                'label' => 'Stadt',
                'required' => false
                ])
            ->add('state', TextType::class, [
                'label' => 'Kanton',
                'required' => false
                ])
            ->add('country', TextType::class, [
                'label' => 'Land',
                'required' => false
                ])
            ->add('phone', TextType::class, [
                'label' => 'Telefon',
                'required' => false
                ])
            ->add('mobile', TextType::class, [
                'label' => 'Mobil',
                'required' => false
                ])
            ->add('email', TextType::class, [
                'label' => 'E-Mail',
                'required' => false
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }
}