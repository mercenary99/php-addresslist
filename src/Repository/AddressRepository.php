<?php

namespace App\Repository;

use App\Entity\Address;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Address|null find($id, $lockMode = null, $lockVersion = null)
 * @method Address|null findOneBy(array $criteria, array $orderBy = null)
 * @method Address[]    findAll()
 * @method Address[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AddressRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Address::class);
    }

    /**
     * @return Address[] Returns an array of Address objects
     */
    
    public function findByAnyField($value, $userId) {
        return $this->createQueryBuilder('a')
            ->andWhere('a.firstname LIKE :val')
            ->orWhere('a.lastname LIKE :val')
            ->orWhere('a.street LIKE :val')
            ->orWhere('a.zip LIKE :val')
            ->orWhere('a.city LIKE :val')
            ->orWhere('a.country LIKE :val')
            ->orWhere('a.phone LIKE :val')
            ->orWhere('a.mobile LIKE :val')
            ->orWhere('a.email = :val')
            ->andWhere('a.user = :userId')
            ->setParameter('val', '%' . $value . '%')
            ->setParameter('userId', $userId)
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}