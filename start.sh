#!/bin/bash

curl -sS https://get.symfony.com/cli/installer | bash

export PATH="$HOME/.symfony/bin:$PATH"

symfony server:ca:install 

symfony proxy:domain:attach addressbook

symfony proxy:start

symfony server:start